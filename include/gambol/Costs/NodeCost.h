#ifndef COSTS_NODECOST_H_
#define COSTS_NODECOST_H_

#include <string>
#include <ifopt/cost_term.h>

#include <gambol/Variables/NodesVariables.h>

namespace gambol {

/**
 * Cost term based on a node value
 *
 * Cost is the squared sum of node value. One instance of this class
 * governs a single dimension.
 */
    class NodeCost : public ifopt::CostTerm {
    public:
        NodeCost(const std::string& nodes_id, int dim, double weight, double exp =
        2.0, const std::string& prefix = "");

        virtual ~NodeCost() = default;

        void InitVariableDependedQuantities(const VariablesPtr& x) override;

        virtual double GetCost() const override;

    protected:
        NodesVariables::Ptr nodes_;

        std::string node_id_;
        int dim_;
        double weight_;
        double exp_;

        virtual void FillJacobianBlock(std::string var_set, Jacobian&) const
        override;
    };

} /* namespace gambol */

#endif /* COSTS_NODECOST_H_ */
