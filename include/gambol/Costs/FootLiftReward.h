#ifndef COSTS_FOOTLIFTREWARD_H_
#define COSTS_FOOTLIFTREWARD_H_

#include <ifopt/cost_term.h>

#include <gambol/Robots/RobotModel.h>
#include <gambol/Variables/NodesHolder.h>

namespace gambol {

/**
 * Reward (negative cost) for the height of a foot above the ground
 */
    class FootLiftReward : public ifopt::CostTerm {
    public:
        using Vector3d = Eigen::Vector3d;

        FootLiftReward(const NodeTimes::Ptr node_times,
                       const std::vector<PhaseDurations::Ptr>& phase_durations,
                       const RobotModel::Ptr model, uint ee_id, double weight);

        virtual ~FootLiftReward() = default;

        void InitVariableDependedQuantities(const VariablesPtr& x) override;

        virtual double GetCost() const override;

    private:
        mutable RobotModel::Ptr model_;
        NodeTimes::Ptr node_times_;
        NodesVariables::Ptr joint_pos_;
        std::vector<PhaseDurations::Ptr> phase_durations_;
        uint ee_id_;
        double weight_;

        bool Update(int k) const;

        virtual void FillJacobianBlock(std::string var_set, Jacobian&) const
        override;
    };

} /* namespace gambol */

#endif /* COSTS_FOOTLIFTREWARD_H_ */
