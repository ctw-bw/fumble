#ifndef COSTS_NODEDERIVATIVECOST_H_
#define COSTS_NODEDERIVATIVECOST_H_

#include "NodeCost.h"

namespace gambol {

    class NodeDerivativeCost : public NodeCost {
    public:
        NodeDerivativeCost(const std::string& nodes_id, int dim, double weight);

        virtual ~NodeDerivativeCost() = default;

        double GetCost() const override;

        void FillJacobianBlock(std::string var_set, Jacobian& jac) const override;

    };

} /* namespace gambol */

#endif /* COSTS_NODEDERIVATIVECOST_H_ */
