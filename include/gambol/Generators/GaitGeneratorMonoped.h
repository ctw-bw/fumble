#ifndef GAMBOL_MODELS_MONOPED_GAIT_GENERATOR_H_
#define GAMBOL_MODELS_MONOPED_GAIT_GENERATOR_H_

#include "GaitGenerator.h"

namespace gambol {

/**
 * @brief Produces the contact sequence for a variety of one-legged gaits.
 *
 * @sa GaitGenerator for more documentation
 */
    class GaitGeneratorMonoped : public GaitGenerator {
    public:
        GaitGeneratorMonoped() = default;

        virtual ~GaitGeneratorMonoped() = default;

    private:
        GaitInfo GetGait(Gaits gait) const override;

        GaitInfo GetStrideStand() const;

        GaitInfo GetStrideFlight() const;

        GaitInfo GetStrideHop() const;

        GaitInfo GetStrideHopLong() const;

        ContactState o_ = ContactState(1, true);  // stance
        ContactState x_ = ContactState(1, false); // flight

        void SetCombo(Combos combo) override;
    };

} /* namespace gambol */

#endif /* GAMBOL_MODELS_MONOPED_GAIT_GENERATOR_H_ */
