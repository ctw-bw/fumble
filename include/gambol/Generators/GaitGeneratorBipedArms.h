#ifndef GENERATORS_GAITGENERATORBIPEDARMS_H_
#define GENERATORS_GAITGENERATORBIPEDARMS_H_

#include "GaitGeneratorBiped.h"

namespace gambol {

/**
 * @brief Gait generator for a pinfoot biped with arms
 *
 * The arms are almost always in the swing phase.
 *
 * @sa GaitGenerator for more documentation
 */
    class GaitGeneratorBipedArms : public GaitGeneratorBiped {
    public:
        GaitGeneratorBipedArms();

        virtual ~GaitGeneratorBipedArms() = default;

        enum BipedFeetIDs {
            // Left and right feet, left and right hands
            L, R, LH, RH
        };

    protected:

        GaitInfo GetGait(Gaits gait) const override;

        GaitInfo GetStrideHandstand() const;

        // Extend modes from Biped

        ContactState I_B_; // Hand-stand
        ContactState P_B_; // Swing-up right foot
        ContactState b_B_; // Swing-up left foot
        ContactState B_B_; // All fours
    };

} /* namespace gambol */

#endif /* GENERATORS_GAITGENERATORBIPEDARMS_H_ */
