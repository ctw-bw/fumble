#ifndef CONSTRAINTS_TERRAINFLATCONSTRAINT_H_
#define CONSTRAINTS_TERRAINFLATCONSTRAINT_H_

#include "NodesConstraint.h"
#include <gambol/Robots/RobotModel.h>

namespace gambol {

/**
 * Terrain constraint
 *
 * (This is an older version that does not incorporate terrain)
 *
 * Keeps end-effector on the ground with velocity zero during stance
 * and keeps it above ground during swing.
 *
 * 		| '	|	|	| '	|
 * 		| '	|	|	| '	x
 * 		x '	|	|	| '	|
 * 		| '	|	|	| '	|
 * 		| '	x	x	x '	|
 * 		| '	|	|	| '	|
 * 	(sw)	   (st)	     (sw)
 *
 * The constraints for each node are:
 * 			z_ee_k
 * 		x_ee_k - x_ee_k-1
 * 		y_ee_k - y_ee_k-1
 *
 * During swing the bounds are (0,inf) and (-inf,inf) respectively.
 * During stance the bounds are (0,0) for both.
 * _Except_ for the first node of a stance phase! For this node the
 * x-position constraint is also (-inf,inf).
 */
    class TerrainFlatConstraint : public NodesConstraint {
    public:
        using Vector3d = Eigen::Vector3d;

        TerrainFlatConstraint(const RobotModel::Ptr& model,
                              const NodesHolder& nodes_holder, uint ee_id);

        virtual ~TerrainFlatConstraint() = default;

        bool Update(int k) const;

        virtual void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        virtual void UpdateBoundsAtNode(int k, VecBound& b) const override;

        virtual void UpdateJacobianAtNode(int k, std::string var_set,
                                          Jacobian& jac) const override;

    private:
        int GetRow(int k, int type) const;

        uint ee_id_;
        mutable RobotModel::Ptr model_; ///< Robot model used for kinematics
        mutable RobotModel::Ptr model_km1_;
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_TERRAINFLATCONSTRAINT_H_ */
