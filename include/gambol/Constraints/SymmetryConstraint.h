#ifndef CONSTRAINTS_SYMMETRYCONSTRAINT_H_
#define CONSTRAINTS_SYMMETRYCONSTRAINT_H_

#include <ifopt/constraint_set.h>

#include <gambol/Variables/NodesVariables.h>

namespace gambol {

/**
 * Constraint to keep the initial pose identical the last pose
 */
    class SymmetryConstraint : public ifopt::ConstraintSet {
    public:
        using VectorXd = Eigen::VectorXd;
        using VecTimes = std::vector<double>;
        using Bounds = ifopt::Bounds;

        SymmetryConstraint(const NodesVariables::Ptr& nodes, const std::vector<int>& dims = {});

        virtual ~SymmetryConstraint() = default;

        VectorXd GetValues() const override;

        VecBound GetBounds() const override;

        void FillJacobianBlock(std::string var_set, Jacobian& jac) const override;

    private:

        NodesVariables::Ptr nodes_;
        std::vector<int> dims_;
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_SYMMETRYCONSTRAINT_H_ */
