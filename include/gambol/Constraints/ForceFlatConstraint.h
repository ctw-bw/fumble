#ifndef CONSTRAINTS_FORCEFLATCONSTRAINT_H_
#define CONSTRAINTS_FORCEFLATCONSTRAINT_H_

#include "NodesConstraint.h"

namespace gambol {

    /**
     * Constraint for ground reaction forces
     *
     * (This is an older version that does not incorporate terrain)
     *
     * This constraint does two things:
     *  - Keep force zero during swing phase
     *  - Limit force to positive normal and pyramid friction cone
     */
    class ForceFlatConstraint : public NodesConstraint {
    public:
        ForceFlatConstraint(const NodesHolder& nodes_holder, double max_normal,
                            uint ee_id);

        virtual ~ForceFlatConstraint() = default;

        virtual void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        virtual void UpdateBoundsAtNode(int k, VecBound& b) const override;

        virtual void UpdateJacobianAtNode(int k, std::string var_set,
                                          Jacobian& jac) const override;

    private:
        int GetRow(int k, int type) const;

        uint ee_id_;
        double max_normal_force_;
        double mu_; ///< Friction coefficient
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_FORCEFLATCONSTRAINT_H_ */
