#ifndef CONSTRAINTS_QUATERNIONCONSTRAINT_H_
#define CONSTRAINTS_QUATERNIONCONSTRAINT_H_

#include "NodesConstraint.h"
#include <gambol/Robots/RobotModel.h>

namespace gambol {

/**
 * Constraint quaternions to unity
 *
 * MuJoCos qpos contains quaternions, which need to be constraint to
 * unity length.
 * Note that internally the quaternions are scaled to 1.0 already,
 * nonetheless it seems wise to constrain the optimisation variables too.
 */
    class QuaternionConstraint : public NodesConstraint {
    public:
        QuaternionConstraint(const RobotModel::Ptr& model,
                             const NodesHolder& nodes_holder);

        virtual ~QuaternionConstraint() = default;

    private:
        /** Return index of constraint based on node info */
        int GetRow(int k, int quat = 0) const;

        /** Extract states for current node */
        bool Update(int k) const;

        virtual void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        virtual void UpdateBoundsAtNode(int k, VecBound& b) const override;

        virtual void UpdateJacobianAtNode(int k, std::string var_set,
                                          Jacobian& jac) const override;

        RobotModel::Ptr model_; ///< Robot model
        NodesVariables::Ptr joint_pos_;

        std::vector<int> quat_ids_;
        int num_quats_;
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_QUATERNIONCONSTRAINT_H_ */
