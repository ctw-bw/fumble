#ifndef CONSTRAINTS_FORCECONSTRAINT_H_
#define CONSTRAINTS_FORCECONSTRAINT_H_

#include "NodesConstraint.h"
#include <gambol/Robots/RobotModel.h>
#include <gambol/Terrain/HeightMap.h>

namespace gambol {

    /**
     * Constraint for ground reaction forces
     *
     * This constraint does two things:
     *  - Keep force zero during swing phase
     *  - Limit force to positive normal and pyramid friction cone
     */
    class ForceConstraint : public NodesConstraint {
    public:
        ForceConstraint(const HeightMap::Ptr& terrain,
                        const RobotModel::Ptr& model,
                        const NodesHolder& nodes_holder, double max_normal, uint ee_id);

        virtual ~ForceConstraint() = default;

        virtual void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        virtual void UpdateBoundsAtNode(int k, VecBound& b) const override;

        virtual void UpdateJacobianAtNode(int k, std::string var_set,
                                          Jacobian& jac) const override;

    private:
        int GetRow(int k, int type = 0) const;

        void Update(int k) const;

        HeightMap::Ptr terrain_;
        mutable RobotModel::Ptr model_; // Model is need to find ee_pos

        uint ee_id_;
        double max_normal_force_;
        double mu_; ///< Friction coefficient
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_FORCECONSTRAINT_H_ */
