#ifndef CONSTRAINTS_DYNAMICSCONSTRAINT_H_
#define CONSTRAINTS_DYNAMICSCONSTRAINT_H_

#include "NodesConstraint.h"
#include <gambol/Robots/RobotModel.h>

namespace gambol {

/**
 * Constraint to keep dynamics correct
 *
 * Dynamics is computed as:
 *
 * 		ddq[k] = dynamics[k] = M[k]^-1 * net_torque[k]
 *
 * Constraint value is integrated velocity violation:
 *
 * 		dq[k] - dq[k+1] + dt/2 * (ddq[k] + ddq[k+1])
 */
    class DynamicsConstraint : public NodesConstraint {
    public:
        DynamicsConstraint(const RobotModel::Ptr& model,
                           const NodesHolder& nodes_holder);

        virtual ~DynamicsConstraint() = default;

    private:
        /** Return index of constraint based on node info */
        int GetRow(int k, int dim = 0) const;

        /** Extract states for current node */
        bool Update(int k) const;

        virtual void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        virtual void UpdateBoundsAtNode(int k, VecBound& b) const override;

        virtual void UpdateJacobianAtNode(int k, std::string var_set,
                                          Jacobian& jac) const override;

        mutable RobotModel::Ptr model_k_; ///< Robot model on which dynamics is based
        mutable RobotModel::Ptr model_kp1_; ///< Robot model on which dynamics is based

        int size_q_, size_dq_;

        // `mutable` to ignore const flag
        mutable double dt_k_;
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_DYNAMICSCONSTRAINT_H_ */
