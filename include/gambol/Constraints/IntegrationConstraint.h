#ifndef CONSTRAINTS_INTEGRATIONCONSTRAINT_H_
#define CONSTRAINTS_INTEGRATIONCONSTRAINT_H_

#include "NodesConstraint.h"
#include <gambol/Robots/RobotModel.h>

namespace gambol {

/**
 * Constraint to ensure velocity between steps integrates to position
 *
 * Constraint value is integration error:
 *
 * 		q[k] - q[k+1] + dt/2 * (dq[k] + dq[k+1])
 *
 * Note that for some models v != dq/dt. Hence a jacobian is used from
 * the robot model:
 *
 *		q[k] - q[k+1] + dt/2 * (B[k] * v[k] + B[k+1] * v[k+1])
 *
 * Where
 *
 * 		dq(t)/dt = B(t) * v(t)
 */
    class IntegrationConstraint : public NodesConstraint {
    public:
        using MatrixXd = Eigen::MatrixXd;

        IntegrationConstraint(const RobotModel::Ptr& model,
                              const NodesHolder& nodes_holder);

        virtual ~IntegrationConstraint() = default;

    private:
        int size_q_, size_dq_;

        /** Return index of constraint based on node info */
        int GetRow(int k, int dim = 0) const;

        /** Extract states for current node */
        bool Update(int k) const;

        virtual void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        virtual void UpdateBoundsAtNode(int k, VecBound& b) const override;

        virtual void UpdateJacobianAtNode(int k, std::string var_set,
                                          Jacobian& jac) const override;

        // `mutable` to ignore const flag
        mutable double dt_k_;
        mutable RobotModel::Ptr model_k_; ///< Robot model on which dynamics is based
        mutable RobotModel::Ptr model_kp1_; ///< Robot model on which dynamics is based
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_INTEGRATIONCONSTRAINT_H_ */
