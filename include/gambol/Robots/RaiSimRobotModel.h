#ifndef ROBOTS_RAISIMROBOTMODEL_H_
#define ROBOTS_RAISIMROBOTMODEL_H_

#include <Eigen/Dense>
#include <vector>
#include <string>
#include "RobotModel.h"
#include <gambol/Variables/NodesHolder.h>
#include <raisim/World.hpp>

namespace gambol {

    /**
     * Robot model implemented through RaiSim articulated system
     *
     * The generalized coordinates are called that literally (q, qpos)
     * The generalized velocity is also called exactly that (dq, qvel)
     */
    class RaiSimRobotModel : public RobotModel {

    public:

        using Ptr = std::shared_ptr<RaiSimRobotModel>;

        /** Constructor */
        RaiSimRobotModel(const std::string& urdf_file, int size_q,
                         int size_dq, int size_u,
                         const std::vector<std::string>& ee_names,
                         const Eigen::MatrixXd& S = Eigen::MatrixXd(0, 0));

        /** Copy constructor */
        RaiSimRobotModel(const RaiSimRobotModel&) = delete;

        /** Destructor */
        virtual ~RaiSimRobotModel() = default;

        /** Clone method */
        RobotModel::Ptr clone() const override;

        /** Write new selection matrix */
        void SetSelectionMatrix(Eigen::MatrixXd S = Eigen::MatrixXd(0, 0));

        /**
         * Get forward dynamics
         */
        VectorXd GetDynamics() const override;

        /**
         * Get current dynamics jacobian w.r.t. position
         */
        Jacobian GetDynamicsJacobianWrtPos() const override;

        /**
         * Get current dynamics jacobian w.r.t. velocity
         */
        Jacobian GetDynamicsJacobianWrtVel() const override;

        /**
         * Get current dynamics jacobian w.r.t. torque
         */
        Jacobian GetDynamicsJacobianWrtTorque() const override;

        /**
         * Get current dynamics jacobian w.r.t. forces
         *
         * The end-effector forces are stacked together, so this jacobian has dimensions:
         *      ( n_ee * 3 x n_dof )
         */
        Jacobian GetDynamicsJacobianWrtForces(uint ee_id) const override;

        /**
	     * Get position in world coordinates of end-effector
	     */
        Vector3d GetEEPos(uint ee_id) const override;

        /**
         * Get jacobian of end-effector position to joint positions
         */
        Jacobian GetEEPosJacobian(uint ee_id) const override;

        /**
         * Enable or disable collision detection between the robot and its surroundings
         *
         * @param enable
         */
        void EnableContact(bool enable);

        /** Get const reference to RaiSim world */
        raisim::World& GetWorld();

        /** Get const pointer to RaiSim system (the robot itself) */
        raisim::ArticulatedSystem* GetSystem();

        /** Get pointer to RaiSim ground object */
        raisim::Ground* GetGround();

    protected:

        /**
         * Propagate states through model
         */
        void Update() override;

        /**
         * Get frame belonging to end-effector
         */
        const raisim::CoordinateFrame& GetEEFrame(uint ee_id) const;

        /**
         * Set actuator torque in system_
         */
        void SetSystemActuatorTorque(const VectorXd& u) const;

        /**
         * Set end-effector forces to the raisim system_
         *
         * @param forces    Set forces as a list of vectors
         */
        void SetSystemEEForces(const std::vector<VectorXd>& forces) const;

        /**
         * Raisim simulation world
         *
         * `mutable` because updating its information requires writing.
         */
        mutable raisim::World world_;

        /**
         * The actual URDF robot
         */
        mutable raisim::ArticulatedSystem* system_;

        /**
         * Ground object (needed to save for visualization)
         */
        mutable raisim::Ground* ground_;

        /**
         * URDF file used to create robot
         */
        std::string file_;

        /**
         * End-effector names (needed for clone method)
         */
        std::vector<std::string> ee_names_;

        /**
         * External forces on the system, mirror to ee_forces_
         *
         * A raisim system does not remember external forces. So instead we track our own list of forces that we
         * actually apply at the start of GetDynamics(). This is separate from ee_forces_ to make e.g. the dynamics
         * jacobian easier.
         */
        mutable std::vector<VectorXd> system_ee_forces_;

        /**
         * Track whether RaiSim activation has been performed at all
         */
        static bool activated;

        const double eps = 1e-5; ///< Finite-difference step

        /**
         * Joint actuator selection matrix
         *
         * Used like:
         * \f[
         *      \tau = S_a * \tau_a
         * \f]
         * There is no convenient way of relating joints and actuators, so this explicit matrix is used instead.
         * By default the selection matrix will skip the first 6 DOF if a floating joint is registered. In case more
         * joints are passive, overwrite this matrix.
         */
        Eigen::MatrixXd actuator_selector_;

        /**
         * List of frame ids corresponding to end-effectors
         */
        std::vector<size_t> ee_frame_ids_;

        /**
         * List of references to the frames of end-effectors
         *
         * Pointers actually, because a vector of references is not allowed.
         */
        std::vector<const raisim::CoordinateFrame*> ee_frames_;
    };

} /* namespace gambol */

#endif /* ROBOTS_RAISIMROBOTMODEL_H_ */
