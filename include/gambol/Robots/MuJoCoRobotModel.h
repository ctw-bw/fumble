#ifndef ROBOTS_MUJOCOROBOTMODEL_H_
#define ROBOTS_MUJOCOROBOTMODEL_H_

#include <Eigen/Dense>
#include <vector>
#include <string>
#include "RobotModel.h"
#include <gambol/Variables/NodesHolder.h>
#include <MuJoCoTools/MuJoCoModel.h>

namespace gambol {

    /**
     * Robot model defined through MuJoCo
     *
     * Use the ee-names to mark the end-effectors. The names must be body names
     * and the body frame must be at the tip of the end-effector.
     */
    class MuJoCoRobotModel : public RobotModel {
    public:
        /** Construct model from file */
        MuJoCoRobotModel(const std::string& file, int size_q, int size_dq,
                         int size_u, const std::vector<std::string>& ee_names);

        MuJoCoRobotModel(const MuJoCoRobotModel&) = default;

        MuJoCoRobotModel::Ptr clone() const override;

        virtual ~MuJoCoRobotModel() = default;

        bool HasIK() const override;

        double GetTotalMass() const override;

        double GetGravity() const override;

        const std::vector<int>& GetEEBodyIds() const;

        /** Get dynamics */
        VectorXd GetDynamics() const override;

        Jacobian GetDynamicsJacobianWrtPos() const override;

        Jacobian GetDynamicsJacobianWrtVel() const override;

        Jacobian GetDynamicsJacobianWrtTorque() const override;

        Jacobian GetDynamicsJacobianWrtForces(uint ee_id) const override;

        Vector3d GetEEPos(uint ee_id) const override;

        Jacobian GetEEPosJacobian(uint ee_id) const override;

        Vector3d GetBasePos() const override;

        Jacobian GetBasePosJacobian() const override;

        VectorXd GetInverseKinematics(const Vector3d& base_pos,
                                      const Vector4d& base_rot, const std::vector<Vector3d>& ee_pos) const override;

        /** Return reference to MuJoCo model */
        MuJoCoModel& GetMuJoCoModel();

    protected:

        /** Propagate states through model, to be executed after SetCurrent() */
        void Update() override;

        /**
         * MuJoCo model object
         *
         * `mutable` since it's internal states are to be updated,
         * regardless of const flags.
         */
        mutable MuJoCoModel model_;

        std::vector<int> ee_body_id_; ///< Body ids of end-effectors
        int base_body_id_; ///< Body id of the base
    };

} /* namespace gambol */

#endif /* ROBOTS_MUJOCOROBOTMODEL_H_ */
