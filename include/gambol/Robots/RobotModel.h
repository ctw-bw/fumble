#ifndef ROBOTS_ROBOTMODEL_H_
#define ROBOTS_ROBOTMODEL_H_

#include <memory>

#include <gambol/Variables/NodesHolder.h>

namespace gambol {

    /**
     * Class for robot instances that can be inserted into the optimizer
     *
     * Extend this class with your own robots. You could wrap an existing simulator or rely on analytical expressions.
     */
    class RobotModel {
    public:
        using Ptr = std::shared_ptr<RobotModel>;
        using VectorXd = Eigen::VectorXd;
        using Vector3d = Eigen::Vector3d;
        using Vector4d = Eigen::Vector4d;
        using MatrixXd = Eigen::MatrixXd;
        using Jacobian = Eigen::SparseMatrix<double, Eigen::RowMajor>;

        /** Constructor */
        RobotModel(int size_q, int size_dq, int size_u, int ee_count);

        virtual ~RobotModel() = default;

        virtual RobotModel::Ptr clone() const = 0;

        virtual bool HasIK() const;

        /**
         * Bring model up-to-date with current time instance
         */
        void SetCurrent(const VectorXd& q = VectorXd(0), const VectorXd& dq =
        VectorXd(0), const VectorXd& u = VectorXd(0),
                        const std::vector<VectorXd>& ee_forces = {});

        /**
         * Bring model up-to-date with current time instance through holder
         */
        virtual void SetCurrent(const NodesHolder& s, int k);

        /**
         * Bring model up-to-date with current time (not node ID)
         */
        virtual void SetCurrentTime(const NodesHolder& s, double t);

        /** Return number of end-effectors */
        int GetEECount() const;

        /** Return list of ids of quaternions */
        virtual const std::vector<int>& GetQuatIndices() const;

        virtual double GetTotalMass() const;

        virtual double GetGravity() const;

        /**
         * Get current forward dynamics
         */
        virtual VectorXd GetDynamics() const = 0;

        /**
         * Get current dynamics jacobian w.r.t. position
         */
        virtual Jacobian GetDynamicsJacobianWrtPos() const = 0;

        /**
         * Get current dynamics jacobian w.r.t. velocity
         */
        virtual Jacobian GetDynamicsJacobianWrtVel() const = 0;

        /**
         * Get current dynamics jacobian w.r.t. torque
         */
        virtual Jacobian GetDynamicsJacobianWrtTorque() const = 0;

        /**
         * Get current dynamics jacobian w.r.t. forces
         */
        virtual Jacobian GetDynamicsJacobianWrtForces(uint ee_id) const = 0;

        /**
         * Return joint rates (qpos_dot) from joint velocities (qvel)
         */
        virtual VectorXd GetRatesFromVel() const;

        /**
         * Get jacobian of joint rates w.r.t. position (qpos)
         */
        virtual Jacobian GetRatesJacobianWrtPos() const;

        /**
         * Get jacobian of joint rates w.r.t. joint velocities (qvel)
         */
        virtual Jacobian GetRatesJacobianWrtVel() const;

        /**
         * Get jacobian of joint velocities (qvel) w.r.t. to joint rates (qpos_dot)
         */
        virtual Jacobian GetVelJacobianWrtRates() const;

        /**
         * Get position in world coordinates of end-effector
         */
        virtual Vector3d GetEEPos(uint ee_id) const = 0;

        /**
         * Get jacobian of end-effector position to joint positions
         */
        virtual Jacobian GetEEPosJacobian(uint ee_id) const = 0;

        /**
         * Get position in world coordinates of the base
         */
        virtual Vector3d GetBasePos() const;

        /**
         * Get jacobian of end-effector position to joint positions
         */
        virtual Jacobian GetBasePosJacobian() const;

        /**
         * Get body pose through inverse kinematics
         */
        virtual VectorXd GetInverseKinematics(const Vector3d& base_pos,
                                              const Vector4d& base_rot,
                                              const std::vector<Vector3d>& ee_pos) const;

        /**
         * Propagate states through model
         */
        virtual void Update() = 0;

        /**
         * Turn dense matrix into a sparse one (keeping zero values!)
         */
        static Jacobian DenseToSparse(Eigen::MatrixXd mat);

    protected:

        /**
         * @name Methods to do quaternion magic
         *
         * This is focused on quaternions and angular velocities in body-fixed
         * frame, for quaternions like [w, x, y, z].
         */
        //@{
        /** Calculate quaternion derivative */
        Vector4d QuaternionDerivative(const Vector4d& quat, const Vector3d& omega) const;

        /** Get 4x3 skew form of quaternion */
        static MatrixXd QuaternionMatrix(const Vector4d& quat);

        /** Get 4x3 skew form of quaternion (transpose) */
        MatrixXd QuaternionMatrixTranspose(const Vector4d& quat) const;

        /** Get 4x4 skew form of angular velocity */
        static MatrixXd AngularVelocityMatrix(Vector3d omega);

        //@}

        int size_q_, size_dq_, size_u_;
        int ee_count_;

        VectorXd q_; ///< Generalized coordinates
        VectorXd dq_; ///< Generalized velocity
        VectorXd u_; ///< Joint torques (_not_ generalized force)

        std::vector<VectorXd> ee_forces_; ///< End-effector forces

        /**
         * Indices in gen. coordinates that correspond to the start of a quaternion
         *
         * Can simply be empty if there are no quaternions involved.
         */
        std::vector<int> quat_ids_;
    };

} /* namespace gambol */

#endif /* ROBOTS_ROBOTMODEL_H_ */
