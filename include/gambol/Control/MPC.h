#ifndef CONTROL_MPC_H_
#define CONTROL_MPC_H_

#include <gambol/Problem/NlpFormulation.h>
#include <ifopt/ipopt_solver.h>

namespace gambol {

/**
 * Wrapper for model predictive control for simulation
 *
 * Functionality is fairly basic, main purpose is to wrap the
 * optimization update.
 */
    class MPC {
    public:
        using VectorXd = Eigen::VectorXd;

        MPC();

        virtual ~MPC() = default;

        void SetInitialConfig(const VectorXd& qpos, const VectorXd& qvel);

        void SetTime(double t);

        bool Predict(NodesHolder& solution);

        NlpFormulation formulation_;
        ifopt::IpoptSolver::Ptr solver_;
        ifopt::Problem nlp_;
        int optimizations_;

    private:

        void InterpolateOldSolution(ifopt::VariableSet::Ptr set,
                                    const NodesHolder& old_solution);

        NodesVariables::Ptr GetVariableFromHolder(const NodesHolder& holder,
                                                  const std::string& name) const;

        double t_last_;            ///< Simulation time of last update
        double delta_t_;        ///< Simulation time difference between last updates
    };

} /* namespace gambol */

#endif /* CONTROL_MPC_H_ */
