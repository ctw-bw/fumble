#ifndef CONTROL_ROBOTSTATE_H_
#define CONTROL_ROBOTSTATE_H_

#include <Eigen/Dense>

namespace gambol {

/**
 * Simple struct which completely defines a robot in a frame
 */
    struct RobotState {
        using VectorXd = Eigen::VectorXd;

        RobotState() = default;

        RobotState(const VectorXd& _qpos, const VectorXd& _qvel,
                   const VectorXd& _u_ff, const VectorXd& _u_fb);

        virtual ~RobotState() = default;

        VectorXd qpos;
        VectorXd qvel;
        VectorXd u_ff;
        VectorXd u_fb;
    };

} /* namespace gambol */

#endif /* CONTROL_ROBOTSTATE_H_ */
