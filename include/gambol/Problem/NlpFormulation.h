#ifndef PROBLEM_NLPFORMULATION_H_
#define PROBLEM_NLPFORMULATION_H_

#include <ifopt/constraint_set.h>
#include <ifopt/cost_term.h>
#include <ifopt/ipopt_solver.h>

#include "Parameters.h"
#include <gambol/Variables/NodesHolder.h>
#include <gambol/Variables/NodesVariables.h>
#include <gambol/Variables/NodeTimes.h>
#include <gambol/Variables/PhaseDurations.h>
#include <gambol/Robots/RobotModel.h>
#include <gambol/Terrain/HeightMap.h>

namespace gambol {

/**
 * A combination of variablesets and constraints
 *
 * This class is bascially a factory for NLP, to be solved by IFOPT.
 */
    class NlpFormulation {
    public:
        using VariablePtrVec = std::vector<ifopt::VariableSet::Ptr>;
        using ContraintPtrVec = std::vector<ifopt::ConstraintSet::Ptr>;
        using CostPtrVec = std::vector<ifopt::CostTerm::Ptr>;
        using VectorXd = Eigen::VectorXd;
        using Vector3d = Eigen::Vector3d;
        using Vector4d = Eigen::Vector4d;

        NlpFormulation();

        virtual ~NlpFormulation() = default;

        /** The ifopt variable sets that will be optimized over */
        VariablePtrVec GetVariableSets(NodesHolder& nodes_holder);

        /** The ifopt constraints */
        ContraintPtrVec GetConstraints(const NodesHolder& nodes_holder) const;

        /** The ifopt costs */
        ContraintPtrVec GetCosts() const;

        /** Set typical solver options */
        static void setSolverOptions(const ifopt::IpoptSolver::Ptr& solver);

        /** Parameters of optimisation */
        Parameters params_;

        /** Underlying robot model */
        RobotModel::Ptr model_;

        /** Terrain object */
        HeightMap::Ptr terrain_;

        int size_q_;    ///< Number of states
        int size_dq_;    ///< Number of velocity/acceleration states
        int size_u_;    ///< Number of torque inputs

        VectorXd initial_joint_pos_, initial_joint_vel_;    ///< Initial joint position
        VectorXd final_joint_pos_, final_joint_vel_;        ///< Final joint position
        std::vector<int> bound_initial_joint_pos_, bound_initial_joint_vel_;///< Indices of joints to bound
        std::vector<int> bound_final_joint_pos_, bound_final_joint_vel_; ///< Indices of joints to bound

        std::vector<std::pair<double, double>> joint_pos_limits_; ///< Joint limits
        std::vector<int> bound_joint_pos_limits_; ///< Indices of joints which to limit
        std::vector<std::pair<double, double>> torque_limits_; ///< Torque limits
        std::vector<int> bound_torque_limits_; ///< Indices of torques to limit
        std::vector<int> bound_symmetry_joint_pos_; ///< Indices of joint to fix for symmetry

        bool initial_zpos_; ///< True of initial z-pos is bound

        NodeTimes::Ptr node_times_; ///< Original node_times object
        std::vector<PhaseDurations::Ptr> phase_durations_; ///< Original phases object

    private:
        // Variables
        std::vector<NodesVariables::Ptr> MakeVariablesJoints() const;

        NodesVariables::Ptr MakeVariablesTorques() const;

        std::vector<NodesVariables::Ptr> MakeVariablesForces() const;

        NodeTimes::Ptr MakeNodeTimes() const;

        std::vector<PhaseDurations::Ptr> MakePhaseDurations() const;

        // Constraints
        ContraintPtrVec GetConstraint(Parameters::ConstraintName name,
                                      const NodesHolder& splines) const;

        ContraintPtrVec MakeConstraintIntegration(const NodesHolder& s) const;

        ContraintPtrVec MakeConstraintQuaternions(const NodesHolder& s) const;

        ContraintPtrVec MakeConstraintDynamics(const NodesHolder& s) const;

        ContraintPtrVec MakeConstraintTerrain(const NodesHolder& s) const;

        ContraintPtrVec MakeConstraintForces(const NodesHolder& s) const;

        ContraintPtrVec MakeConstraintSymmetry(const NodesHolder& s) const;

        // Costs
        CostPtrVec GetCost(const Parameters::CostName& id, double weight) const;

        CostPtrVec MakeCostTorque(double weight) const;

        CostPtrVec MakeCostJointAcceleration(double weight) const;

        CostPtrVec MakeCostAngularVelocity(double weight) const;

        CostPtrVec MakeCostFootLift(double weight) const;

        // Initial guess
        void SetJointsInitialGuess(NodesHolder& nodes_holder) const;
    };

} // namespace gambol

#endif /* PROBLEM_NLPFORMULATION_H_ */
