#ifndef VARIABLES_NODESHOLDER_H_
#define VARIABLES_NODESHOLDER_H_

#include "NodesVariables.h"
#include "NodeTimes.h"
#include "PhaseDurations.h"

namespace gambol {

    struct NodesHolder {
        /** Default constructor */
        NodesHolder() = default;

        /** Real constructor */
        NodesHolder(NodesVariables::Ptr joint_pos, NodesVariables::Ptr joint_vel,
                    NodesVariables::Ptr torques,
                    const std::vector<NodesVariables::Ptr>& ee_forces,
                    const std::vector<PhaseDurations::Ptr>& phase_durations,
                    NodeTimes::Ptr nodes_times);

        /** Desctructor */
        virtual ~NodesHolder() = default;

        NodesVariables::Ptr joint_pos_;
        NodesVariables::Ptr joint_vel_;
        NodesVariables::Ptr torques_;
        std::vector<NodesVariables::Ptr> ee_forces_;

        std::vector<PhaseDurations::Ptr> phase_durations_;
        NodeTimes::Ptr node_times_;
    };

} /* namespace gambol */

#endif /* VARIABLES_NODESHOLDER_H_ */
