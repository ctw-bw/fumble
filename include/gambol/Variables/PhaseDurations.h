#ifndef VARIABLES_PHASEDURATIONS_H_
#define VARIABLES_PHASEDURATIONS_H_

#include <memory>
#include <vector>

namespace gambol {

/**
 * Class for durations of phases
 *
 * This class determined whether a node is part of stance or swing.
 */
    class PhaseDurations {
    public:
        using Ptr = std::shared_ptr<PhaseDurations>;
        using VecDurations = std::vector<double>;

        PhaseDurations(const VecDurations& initial_durations,
                       bool is_first_phase_in_contact);

        virtual ~PhaseDurations() = default;

        // Get durations
        const VecDurations& GetPhaseDurations() const;

        // Whether in contact for a specific time
        bool IsContactPhase(double t) const;

    private:
        VecDurations durations_; ///< Actual durations
        double t_total_;
        bool initial_contact_state_; ///< True if first phase in contact
    };

} /* namespace gambol */

#endif /* VARIABLES_PHASEDURATIONS_H_ */
