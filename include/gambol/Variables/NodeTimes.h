#ifndef VARIABLES_NODETIMES_H_
#define VARIABLES_NODETIMES_H_

#include <vector>
#include <memory>

namespace gambol {

/**
 * Class for the times of each node (= collocation point)
 *
 * This class is not essential, but it makes it easier to share
 * a single instance of nodes times between different components.
 */
    class NodeTimes {
    public:
        using Ptr = std::shared_ptr<NodeTimes>;
        using VecTimes = std::vector<double>;

        /** Construct with entire list */
        NodeTimes(const VecTimes& times);

        /** Construct based on total time and number of nodes */
        NodeTimes(double t_total, int n_nodes);

        /** Destructor */
        virtual ~NodeTimes() = default;

        /** Return number of nodes */
        int GetNumberOfNodes() const;

        /** Get total duration */
        double GetTotalTime() const;

        /** Get single time */
        double at(int k) const;

        /** Get entire list */
        const VecTimes& GetList() const;

        /** Return id of last node before given time */
        int GetNodeId(double t_des) const;

        /** Return id of last node and interpolation factor */
        int GetNodeId(double t_des, double& interpolation) const;

        /** Get duration of this node and the next */
        double GetDeltaT(int k) const;

    private:
        VecTimes node_times_;
    };

} /* namespace gambol */

#endif /* VARIABLES_NODETIMES_H_ */
