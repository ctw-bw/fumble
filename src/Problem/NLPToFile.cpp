#include <gambol/Problem/NLPToFile.h>

#include <fstream>
#include <iostream>

namespace gambol {

    /**
     * Constructor
     */
    NLPToFile::NLPToFile(ifopt::Problem* nlp, const std::string& filename) {
        filename_ = filename;
        nlp_ = nlp;
    }

    /**
     * Set guess form file
     */
    bool NLPToFile::SetGuessFromFile() {
        std::ifstream file(filename_);

        if (!file.is_open()) {
            std::cout << std::endl << "Error: Failed to open file to load guess..." << std::endl << std::endl;
            return false;
        }

        int size;
        file >> size;

        if (nlp_->GetNumberOfOptimizationVariables() != size) {
            std::cout << std::endl << "Irrelevant NLP file, skipping" << std::endl << std::endl;
            return false;
        }

        Eigen::VectorXd z(size);

        double value;
        int row = 0;
        while (file >> value) {
            if (row >= size) {
                std::cout << std::endl << "Error: Corrupt NLP file" << std::endl << std::endl;
                return false; // Something went wrong
            }

            z[row++] = value;
        }

        nlp_->SetVariables(z.data());

        std::cout << std::endl << "Using guess from NLP file" << std::endl << std::endl;

        return true;
    }

    /**
     * Write current variable values of NLP to file
     */
    bool NLPToFile::WriteFileFromResult() const {
        std::ofstream file(filename_);

        if (!file.is_open()) {
            std::cout << std::endl << "Error: Failed to open NLP file for writing" << std::endl << std::endl;
            return false;
        }

        int size = nlp_->GetNumberOfOptimizationVariables();

        file << size << std::endl;

        Eigen::VectorXd z = nlp_->GetVariableValues();

        file << z << std::endl;

        std::cout << std::endl << "Writing result to NLP file" << std::endl << std::endl;

        return true;
    }

} /* namespace gambol */
