#include <iostream>
#include <gambol/Variables/NodesVariables.h>

namespace gambol {

    NodesVariables::NodesVariables(int n_nodes, int n_dim, std::string variable_id,
                                   NodeTimes::Ptr node_times) :
            VariableSet(n_nodes * n_dim, variable_id) {
        int n_opt_variables = n_nodes * n_dim;

        n_dim_ = n_dim;
        nodes_ = std::vector<VectorXd>(n_nodes, VectorXd::Zero(n_dim));
        bounds_ = VecBound(n_opt_variables, ifopt::NoBound);

        node_times_ = node_times;

        // Size of object is set in initializer list
    }

    // Return node info of an optimisation index
    NodesVariables::NodeValueInfo NodesVariables::GetNodeInfo(int opt_idx) const {
        int dim = opt_idx % GetDim();
        int id = std::floor(opt_idx / GetDim());
        NodeValueInfo nvi(id, dim);

        return nvi;
    }

    // Get index of specific value inside the big stacked opt. vector
    int NodesVariables::GetOptIndex(const NodeValueInfo& nvi_des) const {
        return GetOptIndex(nvi_des.id_, nvi_des.dim_);
    }

    /**
     * Get index of specific value inside the big stacked opt. vector
     *
     * This function simply overload the basic and doesn't require
     * a NVI sturct.
     */
    int NodesVariables::GetOptIndex(int node_id, int dim) const {
        return node_id * GetDim() + dim;
    }

    // Get all optimisation values in a single vector
    Eigen::VectorXd NodesVariables::GetValues() const {
        VectorXd x(GetRows());

        for (int opt_idx = 0; opt_idx < x.rows(); opt_idx++) {
            auto nvi = GetNodeInfo(opt_idx);
            x(opt_idx) = nodes_.at(nvi.id_)(nvi.dim_);
        }

        return x;
    }

    // Set values through single stacked vector
    void NodesVariables::SetVariables(const VectorXd& x) {
        for (int opt_idx = 0; opt_idx < x.rows(); opt_idx++) {
            auto nvi = GetNodeInfo(opt_idx);
            nodes_.at(nvi.id_)(nvi.dim_) = x(opt_idx);
        }
    }

    // Set values through nodes directly
    void NodesVariables::SetVariables(const std::vector<VectorXd>& nodes) {
        assert(nodes.size() == (uint) GetNumNodes() && "Specified nodes do not match");
        assert(nodes.front().size() == GetDim() && "Nodes dimensions do not match");

        nodes_ = nodes;
    }

    // Get dimension of each node
    int NodesVariables::GetDim() const {
        return n_dim_;
    }

    // Get number of nodes
    int NodesVariables::GetNumNodes() const {
        return nodes_.size();
    }

    // Return vector of bounds
    NodesVariables::VecBound NodesVariables::GetBounds() const {
        return bounds_;
    }

    // Return all nodes
    const std::vector<Eigen::VectorXd>& NodesVariables::GetNodes() const {
        return nodes_;
    }

    // Return a single node
    const Eigen::VectorXd& NodesVariables::GetNode(int k) const {
        return nodes_.at(k);
    }

    // Return point through interpolated time
    Eigen::VectorXd NodesVariables::GetPoint(double t) const {
        double factor;
        int k = node_times_->GetNodeId(t, factor);

        //std::cout << "k = " << k << ", factor = " << factor << std::endl;

        return nodes_[k] + (nodes_[k + 1] - nodes_[k]) * factor; // Return linear interpolation
    }

    // Set specific node value
    void NodesVariables::SetNode(int k, const VectorXd& value) {
        nodes_[k] = value;
    }

    /**
     * Return jacobian of a node to the entire variable stack
     *
     * Resulting jacobian has `dim` rows and `dim * n_nodes` columns.
     * It will consist of zeros with an identity block.
     *
     * @param k		ID of the node to which the jacobian is for
     */
    const NodesVariables::Jacobian NodesVariables::GetNodeJacobian(int k) const {
        Jacobian jac(GetDim(), GetRows());

        for (int dim = 0; dim < GetDim(); dim++) {
            // Set block corresponding to this node to 1.0
            int idx = GetOptIndex(k, dim);
            jac.insert(dim, idx) = 1.0;
        }

        // Needed to avoid Eigen::assert failure "wrong storage order" triggered
        // in dynamic_constraint.cc
        //jac.makeCompressed();

        return jac;
    }

    // Set all nodes through linear interpolation
    void NodesVariables::SetByLinearInterpolation(const VectorXd& initial_val,
                                                  const VectorXd& final_val) {
        VectorXd dp = final_val - initial_val;
        int num_nodes = nodes_.size();

        for (int idx = 0; idx < GetRows(); ++idx) {
            auto nvi = GetNodeInfo(idx);

            double factor = nvi.id_ / static_cast<double>(num_nodes - 1);
            VectorXd pos = initial_val + dp * factor;
            nodes_.at(nvi.id_)(nvi.dim_) = pos(nvi.dim_);
        }
    }

    // Set all nodes in a flight or contact phase to a specific value
    void NodesVariables::SetConstantByPhase(const VectorXd& value, bool contact, const PhaseDurations::Ptr phases) {
        for (int idx = 0; idx < GetRows(); ++idx) {
            auto nvi = GetNodeInfo(idx);

            double t = node_times_->at(nvi.id_);

            if (phases->IsContactPhase(t) == contact) {
                nodes_.at(nvi.id_)(nvi.dim_) = value(nvi.dim_);
            }
        }
    }

    // Add bounds to a group of values
    void NodesVariables::AddBounds(int node_id, const std::vector<int>& dimensions,
                                   const VectorXd& val) {
        for (auto dim : dimensions) {
            AddBound(NodeValueInfo(node_id, dim), val(dim));
        }
    }

    // Add bound to a single value, based on node info
    void NodesVariables::AddBound(const NodeValueInfo& nvi_des, double val) {
        for (int idx = 0; idx < GetRows(); idx++) {
            auto nvi = GetNodeInfo(idx);

            if (nvi == nvi_des) {
                bounds_.at(idx) = ifopt::Bounds(val, val);
            }
        }
    }

    // Add bound to last node
    void NodesVariables::AddStartBound(const std::vector<int>& dimensions,
                                       const VectorXd& val) {
        AddBounds(0, dimensions, val);
    }

    // At bound to first node
    void NodesVariables::AddFinalBound(const std::vector<int>& dimensions,
                                       const VectorXd& val) {
        AddBounds(nodes_.size() - 1, dimensions, val);
    }

    /**
     * Set constant limits to all joints
     *
     * This will overwrite Start and Final bounds!
     */
    void NodesVariables::AddGlobalBound(const std::vector<int>& dimensions,
                                        const std::vector<std::pair<double, double>>& limits) {
        for (auto dim : dimensions) {
            for (uint node_id = 0; node_id < nodes_.size(); node_id++) {
                int idx = GetOptIndex(node_id, dim);

                auto lim = limits.at(dim);

                bounds_.at(idx) = ifopt::Bounds(lim.first, lim.second);
            }
        }
    }

    // Constructor for NodeValueInfo
    NodesVariables::NodeValueInfo::NodeValueInfo(int node_id, int node_dim) {
        id_ = node_id;
        dim_ = node_dim;
    }

    // Equality check for NodeValueInfo
    int NodesVariables::NodeValueInfo::operator==(const NodeValueInfo& right) const {
        return (id_ == right.id_) && (dim_ == right.dim_);
    }

} /* namespace gambol */
