#include <numeric>
#include <gambol/Variables/PhaseDurations.h>

namespace gambol {

    /**
     * Constructor
     *
     * @param timings 		List of phase timings
     * @param is_first_phase_in_contact 	`true` to start in contact
     */
    PhaseDurations::PhaseDurations(const VecDurations& timings,
                                   bool is_first_phase_in_contact) {
        durations_ = timings;
        t_total_ = std::accumulate(timings.begin(), timings.end(), 0.0);
        initial_contact_state_ = is_first_phase_in_contact;
    }

    /**
     * Return durations
     */
    const PhaseDurations::VecDurations& PhaseDurations::GetPhaseDurations() const {
        return durations_;
    }

    /**
     * Check whether in contact at a certain time
     */
    bool PhaseDurations::IsContactPhase(double t_des) const {
        bool contact = initial_contact_state_;

        if (t_des <= 0.0) {
            return contact;
        }

        double t = 0.0;
        for (unsigned int i = 0; i < durations_.size(); i++) {
            t += durations_[i]; // t is at start of this state

            if (t >= t_des - 1e-5) {
                break;
            }

            contact = !contact; // Toggle state
        }

        return contact;
    }

} /* namespace gambol */
