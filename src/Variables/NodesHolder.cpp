#include <gambol/Variables/NodesHolder.h>

namespace gambol {

    NodesHolder::NodesHolder(NodesVariables::Ptr joint_pos,
                             NodesVariables::Ptr joint_vel, NodesVariables::Ptr torques,
                             const std::vector<NodesVariables::Ptr>& ee_forces,
                             const std::vector<PhaseDurations::Ptr>& phase_durations,
                             NodeTimes::Ptr nodes_times) {
        joint_pos_ = joint_pos;
        joint_vel_ = joint_vel;
        torques_ = torques;
        ee_forces_ = ee_forces;
        phase_durations_ = phase_durations;
        node_times_ = nodes_times;
    }

} /* namespace gambol */
