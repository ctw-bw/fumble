#include <gambol/Costs/FootLiftReward.h>
#include <gambol/Variables/VariablesNames.h>
#include <iostream>

namespace gambol {

    /**
     * Constructor
     *
     * @param node_times
     * @param phase_durations
     * @param model
     * @param ee_id			End-effector this applies to
     * @param weight		Weight should be positive to make reward
     */
    FootLiftReward::FootLiftReward(const NodeTimes::Ptr node_times,
                                   const std::vector<PhaseDurations::Ptr>& phase_durations,
                                   const RobotModel::Ptr model, uint ee_id, double weight) :
            ifopt::CostTerm("foot-lift-reward-" + ee_id) {
        node_times_ = node_times;
        phase_durations_ = phase_durations;
        model_ = model;
        ee_id_ = ee_id;
        weight_ = -1.0 * weight;
    }

    /**
     * Link variables
     */
    void FootLiftReward::InitVariableDependedQuantities(const VariablesPtr& x) {
        joint_pos_ = x->GetComponent<NodesVariables>(id::joint_pos);
    }

    /**
     * Update the model with the current frame
     *
     * @param k
     * @return bool		Return false if not using this node
     */
    bool FootLiftReward::Update(int k) const {
        double t = node_times_->at(k);
        if (phase_durations_[ee_id_]->IsContactPhase(t)) {
            return false; // In contact
        }

        model_->SetCurrent(joint_pos_->GetNode(k));

        return true;
    }

    /**
     * Get cost value
     */
    double FootLiftReward::GetCost() const {
        double reward = 0.0;

        const int n_nodes = node_times_->GetNumberOfNodes();
        for (int k = 0; k < n_nodes; k++) {
            if (!Update(k)) {
                continue;
            }

            Vector3d pos = model_->GetEEPos(ee_id_);

            reward += pos[2] * weight_; // Add z-position
        }

        return reward;
    }

    /**
     * Fill in jacobian
     */
    void FootLiftReward::FillJacobianBlock(std::string var_set, Jacobian& jac) const {
        if (var_set == id::joint_pos) {
            const int n_nodes = node_times_->GetNumberOfNodes();
            for (int k = 0; k < n_nodes; k++) {
                if (!Update(k)) {
                    continue;
                }

                // Only take z component
                Jacobian jac_ee_z = model_->GetEEPosJacobian(ee_id_).row(2);

                Jacobian jac_segment = jac_ee_z * joint_pos_->GetNodeJacobian(k);

                jac += jac_segment * weight_;
            }
        }
    }

} /* namespace gambol */
