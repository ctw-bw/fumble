#include <gambol/Generators/GaitGeneratorMonoped.h>

#include <cassert>
#include <iostream>

namespace gambol {

    void GaitGeneratorMonoped::SetCombo(Combos combo) {
        switch (combo) {
            case C0:
                SetGaits({Stand, Hop1, Stand});
                break;
            case C1:
                SetGaits({Stand, Hop1, Hop1, Stand});
                break;
            case C2:
                SetGaits({Stand, Hop1, Hop1, Hop1, Stand});
                break;
            case C3:
                SetGaits({Stand, Hop2, Stand});
                break;
            case C4:
                SetGaits({Stand, Hop2, Hop2, Stand});
                break;
            default:
                assert(false);
                std::cout << "Gait not defined\n";
                break;
        }
    }

    GaitGeneratorMonoped::GaitInfo GaitGeneratorMonoped::GetGait(Gaits gait) const {
        switch (gait) {
            case Stand:
                return GetStrideStand();
            case Flight:
                return GetStrideFlight();
            case Hop1:
                return GetStrideHop();
            case Hop2:
                return GetStrideHopLong();
            default:
                assert(false && "Gait not implemented"); // gait not implemented
        }

        return GaitInfo();
    }

    GaitGeneratorMonoped::GaitInfo GaitGeneratorMonoped::GetStrideStand() const {
        auto times = {0.5,};
        auto contacts = {o_,};

        return std::make_pair(times, contacts);
    }

    GaitGeneratorMonoped::GaitInfo GaitGeneratorMonoped::GetStrideFlight() const {
        auto times = {0.5,};
        auto contacts = {x_,};

        return std::make_pair(times, contacts);
    }

    GaitGeneratorMonoped::GaitInfo GaitGeneratorMonoped::GetStrideHop() const {
        auto times = {0.3, 0.3,};
        auto contacts = {o_, x_,};

        return std::make_pair(times, contacts);
    }

    GaitGeneratorMonoped::GaitInfo GaitGeneratorMonoped::GetStrideHopLong() const {
        auto times = {0.2, 0.3,};
        auto contacts = {o_, x_,};

        return std::make_pair(times, contacts);
    }

} /* namespace gambol */
