#include <gambol/Constraints/ForceFlatConstraint.h>
#include <gambol/Variables/VariablesNames.h>

namespace gambol {

    /**
     * Constructor
     */
    ForceFlatConstraint::ForceFlatConstraint(const NodesHolder& nodes_holder,
                                             double max_normal, uint ee_id) :
            NodesConstraint(nodes_holder, "forcelimits-" + id::EEForceNodes(ee_id)) {
        max_normal_force_ = max_normal;
        ee_id_ = ee_id;

        mu_ = 0.5;

        SetRows(5 * GetNumberOfNodes());
        // Three constraints per node (z-value and two for two slopes)
    }

    /**
     * Get row of constraint vector based on node info
     */
    int ForceFlatConstraint::GetRow(int k, int type) const {
        // Only constraints between nodes
        assert(k < GetNumberOfNodes() && "Index exceeds number of constraints");

        return (k * 5) + type;
    }

    /**
     * Get constraint values
     */
    void ForceFlatConstraint::UpdateConstraintAtNode(int k, VectorXd& g) const {
        auto force = nodes_holder_.ee_forces_[ee_id_]->GetNode(k);
        g[GetRow(k, 0)] = force[2]; // z-force
        g[GetRow(k, 1)] = force[0] - mu_ * force[2]; // < 0
        g[GetRow(k, 2)] = force[0] + mu_ * force[2]; // > 0
        g[GetRow(k, 3)] = force[1] - mu_ * force[2]; // < 0
        g[GetRow(k, 4)] = force[1] + mu_ * force[2]; // > 0
    }

    /**
     * Get constraint bounds
     */
    void ForceFlatConstraint::UpdateBoundsAtNode(int k, VecBound& bounds) const {
        // z-position
        double t_k = nodes_holder_.node_times_->at(k);
        bool contact = nodes_holder_.phase_durations_[ee_id_]->IsContactPhase(t_k);

        if (contact) {
            bounds.at(GetRow(k, 0)) = ifopt::BoundGreaterZero; // z
            bounds.at(GetRow(k, 1)) = ifopt::BoundSmallerZero; // slope, < 0
            bounds.at(GetRow(k, 2)) = ifopt::BoundGreaterZero; // slope, > 0
            bounds.at(GetRow(k, 3)) = ifopt::BoundSmallerZero; // slope, < 0
            bounds.at(GetRow(k, 4)) = ifopt::BoundGreaterZero; // slope, > 0
        } else {
            // Swing, no reaction forces
            bounds.at(GetRow(k, 0)) = ifopt::BoundZero;
            bounds.at(GetRow(k, 1)) = ifopt::BoundZero;
            bounds.at(GetRow(k, 2)) = ifopt::BoundZero;
            bounds.at(GetRow(k, 3)) = ifopt::BoundZero;
            bounds.at(GetRow(k, 4)) = ifopt::BoundZero;
        }
    }

    /**
     * Fill jacobian for constraint
     */
    void ForceFlatConstraint::UpdateJacobianAtNode(int k, std::string var_set,
                                                   Jacobian& jac) const {
        auto forces = nodes_holder_.ee_forces_[ee_id_];
        if (var_set == forces->GetName()) {
            int idx_x = forces->GetOptIndex(k, 0);
            int idx_y = forces->GetOptIndex(k, 1);
            int idx_z = forces->GetOptIndex(k, 2);

            jac.coeffRef(GetRow(k, 0), idx_z) = 1.0; // force_z

            jac.coeffRef(GetRow(k, 1), idx_x) = 1.0; // force_x
            jac.coeffRef(GetRow(k, 1), idx_z) = -mu_; // -force_z

            jac.coeffRef(GetRow(k, 2), idx_x) = 1.0; // force_x
            jac.coeffRef(GetRow(k, 2), idx_z) = mu_; // +force_z

            jac.coeffRef(GetRow(k, 3), idx_y) = 1.0; // force_y
            jac.coeffRef(GetRow(k, 3), idx_z) = -mu_; // -force_z

            jac.coeffRef(GetRow(k, 4), idx_y) = 1.0; // force_y
            jac.coeffRef(GetRow(k, 4), idx_z) = mu_; // +force_z
        }
    }

} /* namespace gambol */
