#include <gambol/Constraints/QuaternionConstraint.h>

namespace gambol {

    QuaternionConstraint::QuaternionConstraint(const RobotModel::Ptr& model,
                                               const NodesHolder& s) :
            NodesConstraint(s, "quaternion") {
        model_ = model;
        joint_pos_ = nodes_holder_.joint_pos_;

        quat_ids_ = model_->GetQuatIndices();

        num_quats_ = quat_ids_.size();

        // Constrain each quaternion
        SetRows(GetNumberOfNodes() * num_quats_);
    }

    /**
     * Get row of constraint vector based on node info
     */
    int QuaternionConstraint::GetRow(int k, int quat) const {
        // Only constraints between nodes
        assert(k < GetNumberOfNodes() && "Index exceeds number of constraints");

        return (k * num_quats_) + quat;
    }

    /**
     * Update constraint value at node
     */
    void QuaternionConstraint::UpdateConstraintAtNode(int k, VectorXd& g) const {
        VectorXd pos = joint_pos_->GetNode(k);

        for (int q = 0; q < num_quats_; q++) {
            double length = 0.0;
            for (int i = 0; i < 4; i++) {
                // Squared sum
                int i_q = quat_ids_[q] + i;
                length += pow(pos[i_q], 2);
            }
            g[GetRow(k, q)] = length - 1.0; // Should be zero
        }
    }

    /**
     * Set bound for specific node
     */
    void QuaternionConstraint::UpdateBoundsAtNode(int k, VecBound& bounds) const {
        for (int q = 0; q < num_quats_; q++) {
            bounds[GetRow(k, q)] = ifopt::BoundZero;
        }
    }

    /**
     * Set jacobian for this node
     *
     * `jac` is the jacobian of the entire constraint to the current variable
     * set.
     */
    void QuaternionConstraint::UpdateJacobianAtNode(int k, std::string var_set,
                                                    Jacobian& jac) const {
        if (var_set == joint_pos_->GetName()) {
            VectorXd pos = joint_pos_->GetNode(k);

            for (int q = 0; q < num_quats_; q++) {
                for (int i = 0; i < 4; i++) {
                    int q_i = quat_ids_[q] + i;
                    int idx = joint_pos_->GetOptIndex(k, q_i);

                    jac.coeffRef(GetRow(k, q), idx) = 2.0 * pos[q_i];
                }
            }
        }
    }

} /* namespace gambol */
