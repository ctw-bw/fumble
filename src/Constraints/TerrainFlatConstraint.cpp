#include <gambol/Constraints/TerrainFlatConstraint.h>

namespace gambol {

    /**
     * Constructor
     */
    TerrainFlatConstraint::TerrainFlatConstraint(const RobotModel::Ptr& model,
                                                 const NodesHolder& nodes_holder, uint ee_id) :
            NodesConstraint(nodes_holder,
                            "flat-terrain-ee-" + std::to_string(ee_id)) {
        ee_id_ = ee_id;
        model_ = model;
        // Rely on copy constructor to create a duplicate
        model_km1_ = model->clone();

        /*
         * Three constraints per node (z-position and diff of x- and y-position)
         * Still include first node, but simply keep x and y velocity unbound
         */
        SetRows(3 * GetNumberOfNodes());
    }

    /**
     * Get row of constraint vector based on node info
     */
    int TerrainFlatConstraint::GetRow(int k, int type) const {
        assert(k < GetNumberOfNodes() && "Index exceeds number of constraints");

        return k * 3 + type;
    }

    /**
     * Update state properties for current node
     *
     * @return bool		False if this k should be skipped
     */
    bool TerrainFlatConstraint::Update(int k) const {
        model_->SetCurrent(nodes_holder_, k);
        int km1 = k - 1;
        if (km1 < 0) {
            km1 = 0;
        }
        model_km1_->SetCurrent(nodes_holder_, km1);

        return true;
    }

    /**
     * Get constraint values
     */
    void TerrainFlatConstraint::UpdateConstraintAtNode(int k, VectorXd& g) const {
        Update(k);

        Vector3d ee_pos = model_->GetEEPos(ee_id_);
        Vector3d ee_pos_km1 = model_km1_->GetEEPos(ee_id_);

        g[GetRow(k, 0)] = ee_pos[2]; // z-position
        g[GetRow(k, 1)] = ee_pos[0] - ee_pos_km1[0]; // x-position diff
        g[GetRow(k, 2)] = ee_pos[1] - ee_pos_km1[1]; // y-position diff
    }

    /**
     * Get constraint bounds
     */
    void TerrainFlatConstraint::UpdateBoundsAtNode(int k, VecBound& bounds) const {
        const double min_distance_above_terrain = 0.0;
        const double max_distance_above_terrain = 1e20; // [m]

        // z-position
        double t_k = nodes_holder_.node_times_->at(k);
        bool contact = nodes_holder_.phase_durations_[ee_id_]->IsContactPhase(t_k);

        bool contact_prev = false;
        if (k > 0) {
            double t_km1 = nodes_holder_.node_times_->at(k - 1);
            contact_prev = nodes_holder_.phase_durations_[ee_id_]->IsContactPhase(
                    t_km1);
        }

        if (contact) {
            bounds.at(GetRow(k, 0)) = ifopt::BoundZero;

            if (contact_prev) {
                bounds.at(GetRow(k, 1)) = ifopt::BoundZero;
                bounds.at(GetRow(k, 2)) = ifopt::BoundZero;
            } else {
                // Very first node and each first node of contact is unbound (for x- and y-position)
                bounds.at(GetRow(k, 1)) = ifopt::NoBound;
                bounds.at(GetRow(k, 2)) = ifopt::NoBound;
            }
        } else {
            bounds.at(GetRow(k, 0)) = ifopt::Bounds(min_distance_above_terrain,
                                                    max_distance_above_terrain);
            bounds.at(GetRow(k, 1)) = ifopt::NoBound;
            bounds.at(GetRow(k, 2)) = ifopt::NoBound;
        }
    }

    /**
     * Fill jacobian for constraint
     */
    void TerrainFlatConstraint::UpdateJacobianAtNode(int k, std::string var_set,
                                                     Jacobian& jac) const {
        Update(k);

        int n = jac.cols(); // Number of columns of jacobian (= size of variable set)
        Jacobian jac_model_1(1, n); // First constraint
        Jacobian jac_model_2(1, n); // Second constraint
        Jacobian jac_model_3(1, n); // Third constraint

        // 19 errors

        auto joint_pos = nodes_holder_.joint_pos_;
        if (var_set == joint_pos->GetName()) {
            Jacobian ee_diff_q = model_->GetEEPosJacobian(ee_id_);
            Jacobian ee_diff_q_km1 = model_km1_->GetEEPosJacobian(ee_id_);

            Jacobian z_ee_diff_q = ee_diff_q.row(2);
            Jacobian x_ee_diff_q = ee_diff_q.row(0);
            Jacobian y_ee_diff_q = ee_diff_q.row(1);
            Jacobian x_ee_diff_q_km1 = ee_diff_q_km1.row(0);
            Jacobian y_ee_diff_q_km1 = ee_diff_q_km1.row(1);

            int km1 = k - 1;
            if (km1 < 0) {
                km1 = 0;
            }

            Jacobian pos_jac_k = joint_pos->GetNodeJacobian(k);
            Jacobian pos_jac_km1 = joint_pos->GetNodeJacobian(km1);

            jac_model_1 = z_ee_diff_q * pos_jac_k;

            jac_model_2 = x_ee_diff_q * pos_jac_k - x_ee_diff_q_km1 * pos_jac_km1;

            jac_model_3 = y_ee_diff_q * pos_jac_k - y_ee_diff_q_km1 * pos_jac_km1;
        }

        jac.row(GetRow(k, 0)) = jac_model_1;
        jac.row(GetRow(k, 1)) = jac_model_2;
        jac.row(GetRow(k, 2)) = jac_model_3;
    }

} /* namespace gambol */
