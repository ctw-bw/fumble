#include <gambol/Constraints/NodesConstraint.h>

namespace gambol {

    /**
     * Constructor
     */
    NodesConstraint::NodesConstraint(const NodesHolder& s, const std::string& name) :
            ConstraintSet(kSpecifyLater, name) {
        nodes_holder_ = s;
    }

    /**
     * Return number of nodes
     */
    int NodesConstraint::GetNumberOfNodes() const {
        return nodes_holder_.node_times_->GetNumberOfNodes();
    }

    /**
     * Return vector of constraint values
     */
    NodesConstraint::VectorXd NodesConstraint::GetValues() const {
        VectorXd g = VectorXd::Zero(GetRows());

        for (int k = 0; k < GetNumberOfNodes(); k++) {
            UpdateConstraintAtNode(k, g);
        }

        return g;
    }

    /**
     * Return vector of constraint bounds
     */
    NodesConstraint::VecBound NodesConstraint::GetBounds() const {
        VecBound bounds(GetRows());

        for (int k = 0; k < GetNumberOfNodes(); k++) {
            UpdateBoundsAtNode(k, bounds);
        }

        return bounds;
    }

    /**
     * Return complete constraint jacobian
     */
    void NodesConstraint::FillJacobianBlock(std::string var_set,
                                            Jacobian& jac) const {
        for (int k = 0; k < GetNumberOfNodes(); k++) {
            UpdateJacobianAtNode(k, var_set, jac);
        }
    }

} /* namespace gambol */
