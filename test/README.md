# Unit Tests

Run the tests like:

```
$ mkdir build && cd build
$ cmake ..
$ make
$ make test
```

You might need to create a symbolic link from `./resources` to 
`./buld/resources` to make sure the binary can find the necessary 
resources.
