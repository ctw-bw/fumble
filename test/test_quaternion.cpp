#include <gtest/gtest.h>
#include <iostream>
#include <cassert>
#include <memory>

#include <gambol/Robots/Robot.h>
#include <MuJoCoTools/MuJoCoFunctions.hpp>
#include <MuJoCoTools/MuJoCoViewer.hpp>
#include <gambol/Robots/MuJoCoRobotModel.h>

using namespace gambol;

class MuJoCoRobotTest : public ::testing::Test {
public:

    /**
     * Constructor - Run before every test too
     */
    MuJoCoRobotTest() {
        robot = Robot(Robot::Biped3D);

        qpos = Eigen::VectorXd::Zero(robot.size_q_);
        qvel = Eigen::VectorXd::Zero(robot.size_dq_);
        u = Eigen::VectorXd::Zero(robot.size_u_);

        model = robot.robot_model_;
        auto mujoco_robot = std::dynamic_pointer_cast<MuJoCoRobotModel>(model);
        mj_model = &mujoco_robot->GetMuJoCoModel();
        m = mj_model->get_model();
        d = mj_model->get_data();

        //Eigen::Vector4d quat = MuJoCo::eulerToQuaternion(0.0, 0.0, 0.0);
        //Eigen::Vector3d omega(1.21, -3.21, 0.12);

        Eigen::Vector4d quat = MuJoCo::euler_to_quaternion(Eigen::Vector3d::Random());
        Eigen::Vector3d omega = Eigen::Vector3d::Random() * 10.0;

        //qpos.setZero();
        qpos.setRandom();
        qpos.segment(3, 4) = quat;
        //qvel.setZero();
        qvel.setRandom();
        qvel.segment(3, 3) = omega;

        u.setRandom();
        for (int ee = 0; ee < robot.num_ee_; ee++) {
            ee_forces.push_back(Eigen::VectorXd::Random(3));
        }

        model->SetCurrent(qpos, qvel, u, ee_forces);
    }

    /**
     * Run once at the start of testing (not repeated!)
     */
    static void SetUpTestSuite() {
        // NOLINTNEXTLINE
        srand(12345); // Make random values predictable

        std::cout << std::fixed;
        std::cout << std::setprecision(4);
    }

    Robot robot;
    Eigen::VectorXd qpos, qvel, u;
    std::vector<Eigen::VectorXd> ee_forces;
    RobotModel::Ptr model = nullptr;
    MuJoCoModel* mj_model = nullptr;
    mjData* d = nullptr;
    mjModel* m = nullptr;
};

/**
 * Test quaternions in MuJoCo
 *
 * Verify the quaternion derivative from GetRatesFromVel()
 * is correct by comparing it to mju_derivQuat().
 * And then verify the conversion of a quaternion derivative
 * back to an angular velocity with GetVelJacobianWrtRates().
 */
//void testQuaternionVelocity()
//{
//	Eigen::VectorXd qpos_dt = model->GetRatesFromVel();
//
//	Eigen::VectorXd qpos2 = qpos;
//	const double dt = 1e-7;
//	mj_integratePos(m, qpos2.data(), qvel.data(), dt);
//	Eigen::VectorXd qpos_dt_mj = (qpos2 - qpos) / dt;
//
//	//std::cout << "qpos_dt: " << qpos_dt.transpose() << std::endl;
//	//std::cout << "qpos_dt_mj: " << qpos_dt_mj.transpose() << std::endl;
//
//	double error = (qpos_dt_mj - qpos_dt).norm();
//	assert(error < 1e-5 && "qpos derivative incorrect!");
//
//	// Test conversion from quat_dt to omega
//	Eigen::VectorXd qvel_calc = model->GetVelJacobianWrtRates() * qpos_dt;
//
//	//std::cout << "qvel: " << qvel.transpose() << std::endl;
//	//std::cout << "qvel_calc: " << qvel_calc.transpose() << std::endl;
//
//	error = (qvel - qvel_calc).norm();
//	assert(error < 1e-6 && "Angular vel. incorrect!");
//}

/**
 * Test quaternion in dynamics in MuJoCo
 *
 * Test the dynamics jacobian w.r.t. qpos by comparing
 * it to a finite difference.
 */
//void testQuaternionDynamics()
//{
//	Eigen::VectorXd qacc = model->GetDynamics();
//
//	//std::cout << "qacc: " << qacc.transpose() << std::endl;
//
//	RobotModel::Jacobian qacc_diff_qpos = model->GetDynamicsJacobianWrtPos();
//
//	RobotModel::Jacobian qacc_diff_qpos_num(robot.size_dq_, robot.size_q_);
//
//	const double eps = 1e-6;
//	for (int i_q = 0; i_q < robot.size_q_; i_q++)
//	{
//		Eigen::VectorXd qpos_perturbed = qpos;
//		qpos_perturbed[i_q] += eps;
//		model->SetCurrent(qpos_perturbed, qvel, u, ee_forces);
//		Eigen::VectorXd qacc_perturbed = model->GetDynamics();
//
//		Eigen::VectorXd deriv = (qacc_perturbed - qacc) / eps;
//
//		for (int i_v = 0; i_v < robot.size_dq_; i_v++)
//		{
//			qacc_diff_qpos_num.insert(i_v, i_q) = deriv(i_v);
//		}
//	}
//
//	auto jac_error = qacc_diff_qpos_num - qacc_diff_qpos;
//
//	assert(jac_error.norm() < 1e-6 && "Quaternion derivative incorrect!");
//}

/**
 * Test quaternion in geometric jacobian
 */
TEST_F(MuJoCoRobotTest, test_quat_geom_jacobian) { // NOLINT

	uint ee_id = 0;

	RobotModel::Jacobian ee_pos_jac = model->GetEEPosJacobian(ee_id);
	RobotModel::Jacobian ee_pos_jac_num(3, robot.size_q_);

	auto ee_pos = model->GetEEPos(ee_id);

	// Find jacobian numerically
	const double eps = 1e-6;
	for (int i_q = 0; i_q < robot.size_q_; i_q++)
	{
		Eigen::VectorXd qpos_perturbed = qpos;
		qpos_perturbed[i_q] += eps;
		model->SetCurrent(qpos_perturbed, qvel, u, ee_forces);
		Eigen::VectorXd ee_pos_perturbed = model->GetEEPos(ee_id);

		Eigen::VectorXd deriv = (ee_pos_perturbed - ee_pos) / eps;

		for (int i_p = 0; i_p < 3; i_p++)
		{
			ee_pos_jac_num.insert(i_p, i_q) = deriv(i_p);
		}
	}

	//std::cout << "ee_pos_jac: " << std::endl << ee_pos_jac << std::endl;
	//std::cout << "ee_pos_jac_num: " << std::endl << ee_pos_jac_num << std::endl;

	auto jac_error = ee_pos_jac_num - ee_pos_jac;
	assert(jac_error.norm() < 1e-5 && "EE Jacobian not correct!");
}

//int main()
//{
//	robot = Robot(Robot::Biped3D);
//
//	qpos = Eigen::VectorXd::Zero(robot.size_q_);
//	qvel = Eigen::VectorXd::Zero(robot.size_dq_);
//	u = Eigen::VectorXd::Zero(robot.size_u_);
//
//	model = robot.robot_model_;
//	auto mujoco_robot = std::dynamic_pointer_cast<MuJoCoRobotModel>(model);
//	mj_model = &mujoco_robot->GetMuJoCoModel();
//	m = mj_model->getModel();
//	d = mj_model->getData();
//
//	//Eigen::Vector4d quat = MuJoCo::eulerToQuaternion(0.0, 0.0, 0.0);
//	//Eigen::Vector3d omega(1.21, -3.21, 0.12);
//
//	Eigen::Vector4d quat = MuJoCo::eulerToQuaternion(Eigen::Vector3d::Random());
//	Eigen::Vector3d omega = Eigen::Vector3d::Random() * 10.0;
//
//	//qpos.setZero();
//	qpos.setRandom();
//	qpos.segment(3, 4) = quat;
//	//qvel.setZero();
//	qvel.setRandom();
//	qvel.segment(3, 3) = omega;
//
//	u.setRandom();
//	for (int ee = 0; ee < robot.num_ee_; ee++)
//	{
//		ee_forces.push_back(Eigen::VectorXd::Random(3));
//	}
//
//	model->SetCurrent(qpos, qvel, u, ee_forces);
//
//	testQuaternionVelocity();
//	testQuaternionDynamics();
//	testQuaternionGeometricJacobian();
//
//	std::cout << "Done";
//	return 1;
//}
