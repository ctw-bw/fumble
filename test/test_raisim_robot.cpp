#include <gtest/gtest.h>
#include <string>
#include <vector>
#include <memory>
#include <iostream>

#include <gambol/Robots/RaiSimRobotModel.h>

using namespace gambol;

using Eigen::VectorXd;
using Eigen::Vector3d;
using Eigen::MatrixXd;
using Jacobian = RaiSimRobotModel::Jacobian;
using RaiSimRobotModelPtr = std::shared_ptr<RaiSimRobotModel>;

class RaiSimRobotTest : public ::testing::Test {
public:

    /**
     * Run once at the start of testing (not repeated!)
     */
    static void SetUpTestSuite() {
        // NOLINTNEXTLINE
        srand(12345); // Make random values predictable

        std::cout << std::fixed;
        std::cout << std::setprecision(4);
    }

    static RaiSimRobotModelPtr makeBlock() {
        std::vector<std::string> names = {};
        return std::make_shared<RaiSimRobotModel>("resources/test/block.urdf", 7, 6, 0, names);
    }

    static RaiSimRobotModelPtr makeAnymal() {
        std::vector<std::string> names = {"LF_ADAPTER_TO_FOOT", "RF_ADAPTER_TO_FOOT", "LH_ADAPTER_TO_FOOT",
                                          "RH_ADAPTER_TO_FOOT"};
        return std::make_shared<RaiSimRobotModel>("resources/test/anymal/anymal.urdf", 19, 18, 12, names);
    }

    static RaiSimRobotModelPtr makePendulum() {
        std::vector<std::string> names = {"link_ee_joint"};
        return std::make_shared<RaiSimRobotModel>("resources/test/double_pendulum.urdf", 2, 2, 2, names);
    }

    static RaiSimRobotModelPtr makeFlywheel() {
        std::vector<std::string> names = {};
        return std::make_shared<RaiSimRobotModel>("resources/test/flywheel.urdf", 8, 7, 1, names);
    }
};

/**
 * Test object creation of the Anymal robot
 */
TEST_F(RaiSimRobotTest, test_create) { // NOLINT
    RaiSimRobotModel model("resources/test/anymal/anymal.urdf", 19, 18, 12, {});

    // Run any default method to check the object isn't
    // completely messed up:
    EXPECT_DOUBLE_EQ(9.81, model.GetGravity());

    // Access the loaded robot
    EXPECT_EQ(18, model.GetSystem()->getDOF());
}

/**
 * Test dynamics of a free-falling fly wheel
 */
TEST_F(RaiSimRobotTest, test_dynamics) { // NOLINT
    RaiSimRobotModelPtr model = makeFlywheel();

    VectorXd q = model->GetSystem()->getGeneralizedCoordinate().e();
    VectorXd dq = VectorXd::Zero(7);
    VectorXd u = VectorXd::Zero(1);

    u[0] = 1.0; // Apply 1 Nm to the flywheel

    q[2] = 5.0; // Move up in the air

    model->SetCurrent(q, dq, u);

    VectorXd ddq = model->GetDynamics();

    VectorXd ddq_expected(7);
    ddq_expected << 0.0, 0.0, -9.81, -10.0, 0.0, 0.0, 20.0;

    EXPECT_TRUE(ddq_expected.isApprox(ddq)) << "ddq is not close enough to the expected";
}

/**
 * Test invariant affect of getDynamics()
 */
TEST_F(RaiSimRobotTest, test_dynamics_invariant) { // NOLINT
    RaiSimRobotModelPtr model = makeAnymal();

    VectorXd q(19);
    q << 0, 0, 0.54, 1.0, 0.0, 0.0, 0.0, 0.03, 0.4, -0.8, -0.03, 0.4, -0.8, 0.03, -0.4,
            0.8, -0.03, -0.4, 0.8; // Move in a default position
    VectorXd dq = VectorXd::Random(18); // Set random velocities [-1, 1]

    model->SetCurrent(q, dq);

    for (int i = 0; i < 10; i++) { // Get dynamics a bunch of times
        model->GetDynamics();
    }

    // Bypass model and get states directly
    VectorXd q_new = model->GetSystem()->getGeneralizedCoordinate().e();
    VectorXd dq_new = model->GetSystem()->getGeneralizedVelocity().e();

    EXPECT_TRUE(q.isApprox(q_new)) << "Position before is not identical to position after!";
    EXPECT_TRUE(dq.isApprox(dq_new)) << "Velocity before is not identical to velocity after!";
}

/**
 * Test invariant affect of getDynamics() by running two simulations
 */
TEST_F(RaiSimRobotTest, test_dynamics_invariant_simulation) { // NOLINT
    RaiSimRobotModelPtr model1 = makeAnymal();
    RaiSimRobotModelPtr model2 = makeAnymal();

    VectorXd q(19);
    q << 0, 0, 0.54, 1.0, 0.0, 0.0, 0.0, 0.03, 0.4, -0.8, -0.03, 0.4, -0.8, 0.03, -0.4,
            0.8, -0.03, -0.4, 0.8; // Move in a default position
    VectorXd dq = VectorXd::Random(18); // Set random velocities [-1, 1]
    VectorXd u = VectorXd::Zero(12);

    model1->SetCurrent(q, dq, u);
    model2->SetCurrent(q, dq, u);

    for (int i = 0; i < 100; i++) { // Simulate for a bunch of steps

        model1->GetDynamics(); // Leave model2 untouched

        model1->GetWorld().integrate();
        model2->GetWorld().integrate();

        model1->GetWorld().getContactSolver().setOrder(true); // Solver order will be modified by integrate()
        model2->GetWorld().getContactSolver().setOrder(true);

        auto system = model1->GetSystem(); // q_ and dq_ are not updated, so force it
        model1->SetCurrent(system->getGeneralizedCoordinate().e(), system->getGeneralizedVelocity().e(), u);
    }

    // Bypass model and get states directly
    VectorXd q1_new = model1->GetSystem()->getGeneralizedCoordinate().e();
    VectorXd dq1_new = model1->GetSystem()->getGeneralizedVelocity().e();
    VectorXd q2_new = model2->GetSystem()->getGeneralizedCoordinate().e();
    VectorXd dq2_new = model2->GetSystem()->getGeneralizedVelocity().e();

    EXPECT_TRUE(q1_new.isApprox(q2_new))
                        << "Positions in models 1 and 2 are not equal!" << std::endl << q1_new.transpose()
                        << std::endl << q2_new.transpose();
    EXPECT_TRUE(dq1_new.isApprox(dq2_new)) << "Velocities in models 1 and 2 are not equal!" << std::endl
                                           << dq1_new.transpose() << std::endl << dq2_new.transpose();
}

/**
 * Test dynamics in a double link pendulum
 */
TEST_F(RaiSimRobotTest, test_dynamics_pendulum) { // NOLINT
    RaiSimRobotModelPtr model = makePendulum();

    VectorXd q = VectorXd::Zero(2);
    VectorXd dq = VectorXd::Zero(2);
    VectorXd u = VectorXd::Zero(2);
    model->SetCurrent(q, dq, u); // Straight down
    VectorXd ddq_passive = model->GetDynamics();
    EXPECT_DOUBLE_EQ(ddq_passive.norm(), 0.0); // Acceleration should be zero

    q << 0.0, M_PI_2;
    model->SetCurrent(q, dq, u); // At a 90 degree angle
    VectorXd ddq_angle = model->GetDynamics();
    EXPECT_NEAR(ddq_angle[1], -2.3, 0.1); // Second link should be swinging down again
    EXPECT_NEAR(ddq_angle[0], 0.0, 0.1); // First link should stay fairly still

    q << 0.0, 0.0;
    u << -1.0, 1.0;
    model->SetCurrent(q, dq, u); // Straight down but with constant torque
    VectorXd ddq_torque = model->GetDynamics();
    EXPECT_NEAR(ddq_torque[0], -1.6, 0.1); // First link should have some negative acceleration
    EXPECT_NEAR(ddq_torque[1], 2.7, 0.1); // Second link should have some positive acceleration (bigger than link 1)
}

/**
 * Test dynamics jacobians
 */
TEST_F(RaiSimRobotTest, test_dynamics_jac) { // NOLINT
    RaiSimRobotModelPtr model = makePendulum();

    VectorXd q = VectorXd::Zero(2);
    VectorXd dq = VectorXd::Zero(2);
    VectorXd u = VectorXd::Zero(2);
    model->SetCurrent(q, dq, u); // Straight down

    MatrixXd jac_pos(model->GetDynamicsJacobianWrtPos());
    MatrixXd jac_pos_expected(2, 2);
    jac_pos_expected << -5.5, 0.2,
            3.8, -2.5;
    // Diagonal is negative (links swing down again)
    // Bottom left is positive (link 1 sweeps forward)

    EXPECT_TRUE(jac_pos.isApprox(jac_pos_expected, 0.1)) << "Pos. Jacobian is not as expected: " << std::endl
                                                         << jac_pos << std::endl << jac_pos_expected;

    MatrixXd jac_u(model->GetDynamicsJacobianWrtTorque());
    MatrixXd jac_u_expected(2, 2);
    jac_u_expected << 0.8, -0.9,
            -0.9, 1.9; // From analytical model

    EXPECT_TRUE(jac_u.isApprox(jac_u_expected, 0.1)) << "Torque Jacobian is not as expected: " << std::endl
                                                     << jac_u << std::endl << jac_u_expected;

    MatrixXd jac_forces(model->GetDynamicsJacobianWrtForces(0));
    MatrixXd jac_forces_expected(2, 3);
    jac_forces_expected << 0.0, 0.33, 0.0,
            0.0, 0.09, 0.0; // Only Y forces are relevant, those should be positive

    EXPECT_TRUE(jac_forces.isApprox(jac_forces_expected, 0.05)) << "Forces Jacobian is not as expected: " << std::endl
                                                                << jac_forces << std::endl << jac_forces_expected;

    q << M_PI_2, M_PI_2; // Maximize coriolis
    dq << 1.0, 1.0;
    model->SetCurrent(q, dq, u);

    MatrixXd jac_vel(model->GetDynamicsJacobianWrtVel());
    MatrixXd jac_vel_expected(2, 2);
    jac_vel_expected << 0.4, 0.4,
            -0.6, -0.4; // From analytical model

    EXPECT_TRUE(jac_vel.isApprox(jac_vel_expected, 0.1)) << "Vel. Jacobian is not as expected: " << std::endl
                                                         << jac_vel << std::endl << jac_vel_expected;
}

/**
 * Test end-effector position
 */
TEST_F(RaiSimRobotTest, test_end_effector) { // NOLINT
    RaiSimRobotModelPtr model = makePendulum();

    VectorXd q = VectorXd::Zero(2);
    model->SetCurrent(q);

    Vector3d pos1 = model->GetEEPos(0);
    Vector3d pos1_expected(0.0, 0.0, 0.5);
    EXPECT_TRUE(pos1.isApprox(pos1_expected)) << "End-effector position not as expected";

    MatrixXd jac1(model->GetEEPosJacobian(0));
    MatrixXd jac1_expected(3, 2);
    jac1_expected << 0.0, 0.0,
            1.0, 0.5,
            0.0, 0.0;
    EXPECT_TRUE(jac1.isApprox(jac1_expected)) << "End-effector jacobian not as expected";

    q << -M_PI_2, M_PI_2;
    model->SetCurrent(q);

    Vector3d pos2 = model->GetEEPos(0);
    Vector3d pos2_expected(0.0, -0.5, 1.0);
    EXPECT_TRUE(pos2.isApprox(pos2_expected)) << "End-effector position not as expected";

    MatrixXd jac2(model->GetEEPosJacobian(0));
    MatrixXd jac2_expected(3, 2);
    jac2_expected << 0.0, 0.0,
            0.5, 0.5,
            -0.5, 0.0;
    EXPECT_TRUE(jac2.isApprox(jac2_expected)) << "End-effector jacobian not as expected";
}

/**
 * Test end-effector forces
 */
TEST_F(RaiSimRobotTest, test_end_effector_force) { // NOLINT
    RaiSimRobotModelPtr model = makePendulum();

    VectorXd q(2);
    q << M_PI_2, 0.0; // Horizontal
    VectorXd dq = VectorXd::Zero(2);
    VectorXd u(2);
    u << (0.25 * 1.0 + 0.25 * 1.0) * 9.81, 0.0;
    VectorXd F(3);
    F << 0.0, 0.0, 0.5 * 1.0 * 9.81;
    std::vector<VectorXd> forces = {F};

    model->SetCurrent(q, dq, u, forces); // Set should torque and tip force to exactly balance the system

    VectorXd ddq = model->GetDynamics();

    EXPECT_DOUBLE_EQ(0.0, ddq.norm()) << "System should be in equilibrium but acceleration is not zero";
}

/**
 * Test quaternions in derivatives
 */
TEST_F(RaiSimRobotTest, test_quaternion_derivatives) { // NOLINT
    RaiSimRobotModelPtr model = makeBlock();

    auto idx = model->GetQuatIndices();

    ASSERT_EQ(idx.size(), 1);
    EXPECT_EQ(3, idx[0]);

    VectorXd q = model->GetSystem()->getGeneralizedCoordinate().e();
    q[2] = 5.0; // Move up into the air
    VectorXd dq = VectorXd::Random(6); // Set non-zero angular velocity [-1, 1]

    model->SetCurrent(q, dq); // Update properties and real model

    // q_dot = B(q) * v
    VectorXd q_dot = model->GetRatesFromVel();

    model->GetWorld().setGravity({0.0, 0.0, 0.0});
    model->GetWorld().integrate(); // Let velocity integrate

    // Compute quaternion derivative by finite-difference, compare:
    VectorXd q_new = model->GetSystem()->getGeneralizedCoordinate().e();

    VectorXd q_dot_fin_diff = (q_new - q) / model->GetWorld().getTimeStep();

    EXPECT_TRUE(q_dot.isApprox(q_dot_fin_diff, 1e-3)) << "Joint rates are not equal to simulation approximation"
                                                      << std::endl << q_dot.transpose() << std::endl
                                                      << q_dot_fin_diff.transpose();

    // Now compute the gen. velocity from the derivative of the gen. coord.

    Jacobian B_inv = model->GetVelJacobianWrtRates(); // d(v)/d(q_dot) = B_inv(q)
    VectorXd v_computed = B_inv * q_dot;
    EXPECT_TRUE(dq.isApprox(v_computed)) << "Gen. velocity is not equal to the original" << std::endl
                                         << dq.transpose() << std::endl << v_computed.transpose();
}

/**
 * Test clone method
 */
TEST_F(RaiSimRobotTest, test_clone) { // NOLINT
    RaiSimRobotModelPtr model = makeBlock();

    auto model_clone = model->clone();
    RaiSimRobotModelPtr model_clone_raisim = std::dynamic_pointer_cast<RaiSimRobotModel>(model_clone);

    EXPECT_EQ(model->GetSystem()->getName(), model_clone_raisim->GetSystem()->getName());
}