Control
=======

The following classes are used to realise the generated motion on a simulated system.

MPC
---

.. doxygenfile:: Control/MPC.h



Robot State
-----------

.. doxygenfile:: Control/RobotState.h



CsvLogger
---------

.. doxygenfile:: Control/CsvLogger.h
