Robots
======

Model objects are used to simulate dynamics and kinematics. Robot classes have been as factories to
easily switch between different scenarios.

Robot Model
-----------

.. doxygenfile:: Robots/RobotModel.h


MuJoCo Robot Model
------------------

.. doxygenfile:: Robots/MuJoCoRobotModel.h


RaiSim Robot Model
------------------

.. doxygenfile:: Robots/RaiSimRobotModel.h


Robots
------

.. doxygenfile:: Robots/Robot.h
