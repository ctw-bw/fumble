Consts
======

Costs in the NLP are created through the following classes.


Node Cost
---------

.. doxygenfile:: Costs/NodeCost.h


Node Derivative Cost
--------------------

.. doxygenfile:: Costs/NodeDerivativeCost.h


Foot Lift Reward
----------------

.. doxygenfile:: Costs/FootLiftReward.h
