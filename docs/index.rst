.. Gambol documentation master file, created by
   sphinx-quickstart on Thu Jan 21 14:59:08 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gambol's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   self
   api/problem
   api/variables
   api/constraints
   api/costs
   api/robots
   api/generators
   api/terrain
   api/control


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
